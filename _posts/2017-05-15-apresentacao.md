---
layout: post
title:  "Apresentação"
date:   2017-05-15 03:29:00 -0300
---
Bem como sou um nerd a moda antiga, sou fanático por console, tela 80x25, gosto de velharia, gosto de MSX, Apple2, Tk90x, gosto de Homebrew computer (Computadores Feitos em Casa), este é um assunto que pretendo abordar neste blog.

Não sou do tipo que retem informação, gosto de dissemina-la, penso que com isso podemos melhorar o conhecimento de todos, e porque não me corrigirem e melhorarem aquilo que posto aqui, coisa que sempre incentivo a fazerem.

Gosto muito de programar em linguagem assembly, muito dos meus amigos sabem disso e ainda me perguntam porque fazer isso, bem a resposta é simples porque gosto.

Para mim não tem coisa mais legal de saber como funcionam as coisas, como é o processo de boot de um computador, como é escrito um bootloader, como é feita a carga de estágios e por ai vai.

Outra coisa que amo de paixão é Homebrew não só o software mas o hardware também  a possibilidade de construir o próprio computador, desenvolver o software monitor (BIOS).

Bem tentarei abordar tudo isso aqui, perguntas são sempre muito bem vindas.

Grande abraço.
