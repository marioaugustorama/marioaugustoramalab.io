---
layout: post
title:  "Ferramentas de desenvolvimento AVR"
date:   2017-05-15 04:11:00 -0300
tags: desenvolvimento, avr
---

# Ferramentas de desenvolvimento AVR - Instalação para Linux

Bem pessoal este post fornece instruções rápidas de como instalar as ferramentas de desenvolvimento AVR no Linux. Para a plataforma Windows , há o WinAVR contendo todas as mesmas ferramentas. Existem também ports de ferramentas AVR disponíveis para Mac OSX e FreeBSD.Neste laboratório, usaremos o Debian Linux com ferramentas de desenvolvimento GNU AVR. Com as instruções a seguir, vocês poderão instalar as ferramentas correspondentes em seus próprios computadores. Iremos usar o Arduino como Gravador ISP para nossos próximos experimentos.


## GNU toolchain

O que é o Toolchain ? É um conjunto de ferramentas que consiste de compilador, utilitários binários e a biblioteca C. Todas estas ferramentas que compõe um toolchain são o básico para desenvolver para os microcontroladores AVR Atmel.

+ gcc-avr – Compilador GNU C para AVR
+ binutils-avr – Utilitários para Binários (link, assembler, etc) para AVR.
+ avr-libc – Biblioteca C básica para AVR.
+ gdb-avr – Debugger GNU para AVR.


## Debian (apt)

A Instalação das ferramentas GNU é um processo muito simples na plataforma Debian e (derivados do Debian e do Debian: Knoppix, Ubuntu, etc.) O mecanismo apt se encarrega de todo o processo de instalação sem necessidade de baixar os pacotes de instalação manualmente.
Na linha de comando (terminal), execute os seguintes comandos talvez seja necessário digitar sua senha para acesso a root:

+ sudo apt-get install gcc-avr 
+ sudo apt-get install avr-libc 
+ sudo apt-get install gdb-avr

As ferramentas instaladas acima são usadas no processo de desenvolvimento e depuração do código.

Mas também é necessário instalar o software para gravação do seu código no microcontrolador, para isso temos vários softwares que podem ser usados eu particularmente gosto de usar o avrdude, mas há que goste de usar o avrprog, para a instalação execute na linha de comando (terminal):

+ sudo apt-get install avrdude
+ sudo apt-get install avrprog 

Recomendo para ter um ambiente bem atualizado que faça uso de um repositório que fornece componentes mais atualizados.

Siga as instruções no site:
[DotDeb](http://www.dotdeb.org/instructions/)

Por enquanto é isso, na próxima iremos por a mão na massa. 
