---
layout: page
title: Cursos 
permalink: /cursos/
---

# Introdução a programação em Assembly 32-bits
## Periodo: 5 dias - 40 horas

* Introdução a linguagem
* Arquitetura básica X86 e instruções básicas.
* O Stack (Pilha)
* Configurando seu ambiente.
* Endereçamento
* Operações matemáticas básicas.
* Outras Instruções
* Jumps e Loops
* Assembly no Linux
* Interrupções e debug
* Material adicional
